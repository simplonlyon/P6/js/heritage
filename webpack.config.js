module.exports = {
    mode: 'development',
    entry: {
      main: './src/js/index.js',
      chess: './src/js/exercice/exercice-chess.js'
    },
    devtool: 'inline-source-map'
};
