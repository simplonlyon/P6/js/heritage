import { Animal } from "./animal";

export class Herbivor extends Animal {
    /**
     * On redéfinit ici la méthode Animal.eat pour faire que si 
     * l'Animal est une instance de Herbivor, alors il ne mangera
     * que de la nourriture végétale.
     * @param {Food} food la nourriture à manger, on part du principe
     * qu'il nous sera donnée ici une instance de la classe Food,
     * mais en soit rien n'y oblige dans notre code actuel.
     */
    eat(food) {
        if(food.vegetal) {
          super.eat(food);
        } else {
            console.log('I only eat vegetables');
        }
    }
}