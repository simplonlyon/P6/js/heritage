import { Animal } from "./animal";

/**
 * Héritage : 
 * Une classe peut hériter d'une (seule) autre classe en utilisant
 * le mot clef extends.
 * La classe enfant possédera toutes les propriétés et les 
 * méthodes de la classe parente.
 * Ici, le Dog pourra faire tout ce qu'un Animal sait faire (avoir
 * une propriété hunger et avoir une méthode eat())
 * La classe enfant pourra également définir ses propres méthodes
 * et propriétés
 * ici, le Dog a un name et une breed et peut aboyer en plus.
 */
export class Dog extends Animal {
    constructor(name, breed) {
        //lorsqu'on fait une classe enfant, la première chose à
        //faire est d'appeler le constructeur de la classe parent
        //avec le mot clef super comme ci desssous.
        //Si le constructeur parent a des arguments, il faut leur
        //donner des valeur entre les parenthèses du super
        super();
        this.name = name;
        this.breed = breed;
    }

    bark() {
        
        alert('bork bork bork !');
    }
}