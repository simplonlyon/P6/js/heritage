/**
 * Classe représentant un pion d'un jeu d'échec un peu pété
 */
export class Pawn {
    constructor(x = 0, y = 0) {
        this.x = x;
        this.y = y;
    }
    /**
     * Methode qui fait avancer la pion de 1 'case' vers la droite
     */
    move() {
        this.x++;
    }

    /**
     * Méthode qui génère l'élément HTML représentant le pion
     * @returns {HTMLElement} l'élément html
     */
    draw() {
        let pawnHTML = document.createElement('div');
        pawnHTML.classList.add('pawn');

        pawnHTML.style.left = `${this.x*110}px`;
        pawnHTML.style.top = `${this.y*110}px`;

        return pawnHTML;
    }

}