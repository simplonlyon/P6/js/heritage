import { Pawn } from "./pawn";

//l'élément html représentant le plateau de jeu
let board = document.querySelector('#board');
//le bouton html move
let btnMove = document.querySelector('#move');
//les pions de notre jeu, qu'on génère avec la fonction
let myPieces = generatePieces(5);
//le pion actuellement sélectionné, initialisé à null (pasque pas de pion sélectionné par défaut)
let selectedPiece = null;

drawBoard(myPieces);

// let instancePawn = new Pawn(1,1);

// board.appendChild(instancePawn.draw());

btnMove.addEventListener('click', function () {
    selectedPiece.move();
    drawBoard(myPieces);

});
/**
 * Fonction permettant de générer des instances de pions
 * @param {number} count le nombre de pion à générer
 * @returns {Pawn[]} les pions générés par la fonction
 */
function generatePieces(count) {
    let arrPawn = [];

    for (let index = 0; index < count; index++) {
        let newPawn = new Pawn(1, index);
        arrPawn.push(newPawn);
    }

    return arrPawn;

}

/**
 * Fonction permettant d'append les éléments html représentant les
 * pions à la div#board. Cette fonction rajoute également un event
 * listener sur ces éléments html permettant de sélectionner un pion.
 * @param {Pawn[]} pieces Les pions à afficher sur le plateau en html
 */
function drawBoard(pieces) {
    board.innerHTML = "";
    for (const itemPawn of pieces) {
        let drawElem = itemPawn.draw();

        drawElem.addEventListener('click', function () { // () => revient à faire function() ..

            selectedPiece = itemPawn;
            console.log(selectedPiece);

        });
        board.appendChild(drawElem);

    }

}


