
export class Food {
    constructor(calories, vegetal, cooked = false) {
        this.calories = calories;
        this.vegetal = vegetal;
        this.cooked = cooked;
    }
    /**
     * Méthode qui fait cuire la nourriture si elle n'est pas déjà
     * cuite. La cuisson augmente légérement les calories.
     */
    cook() {
        if(!this.cooked) {
        // if(this.cooked === false) {
           this.calories += 15;
           this.cooked = true;
        } else {
            console.log("already cooked");
        }
    }
}