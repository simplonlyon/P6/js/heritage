import { Food } from "./food";


/**
 * On créer une classe Animal qui aura une propriété hunger pour
 * indiquer la faim de l'animal.
 */
export class Animal {
    constructor(hunger = 0) {
        this.hunger = hunger;

    }
    /**
     * Cette méthode permet de faire baisser la faim de l'animal
     * selon ce qu'il mange.
     * @param {Food} food la nourriture que l'animal va manger
     */
    eat(food) {
        //Ici, on dit à notre méthode que si l'argument food fourni
        //n'est pas une instance de la classe Food, alors on fait 
        //planter le programme avec un throw new Error
        if( !(food instanceof Food) ) {
            throw new Error('Invalid argument, Food instance exepected');
        }
        this.hunger -= food.calories;
        // this.hunger = this.hunger - food.calories;
        if(this.hunger < 0) {
            this.hunger = 0;
        }
    }
}