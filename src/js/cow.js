import { Herbivor } from "./herbivor";
import { Food } from "./food";

export class Cow extends Herbivor {

    constructor(paramCalf = false) {
        super();
        this.hasCalf = paramCalf;
    }
    /**
     * Cette méthode permet à la vache de produire du lait si elle
     * a un veau.
     * @returns {Food} Si la vache a un veau, la méthode return une
     * instance de nourriture (du lait), sinon elle return null
     */
    milk() {
        let milk = null;
        if(this.hasCalf) {
           milk = new Food(140, false);
            this.hunger += 140;
        }
        
        return milk;

    }


}
