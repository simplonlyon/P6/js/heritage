import { Animal } from "./animal";


export class SharkTiger extends Animal {

    constructor() {
        super();
        this.teeth = 100;
    }

    swim() {
        this.hunger += 20;
        console.log('I love swimming in the ocean');
    }
    /**
     * Ici, on redéfinit la méthode eat dans l'enfant.
     * Redéfinir une méthode signifie que lorsqu'on fera appel à la
     * méthode eat sur l'enfant, c'est la méthode de l'enfant qui
     * sera appelée et pas celle du parent. On change la manière
     * dont l'enfant mange comparé à son parent
     */
    eat(food) {
        //Rien ne nous empêche ceci dit dans la redéfinition de la
        //méthode de faire appel à la version du parent de la méthode
        //en question, en utilisant le mot clef encore une fois
        //super est un peut comme un this mais qui fait référence à
        //la classe parent
        super.eat(food);
        this.teeth--;
    }
}