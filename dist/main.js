/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/js/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/animal.js":
/*!**************************!*\
  !*** ./src/js/animal.js ***!
  \**************************/
/*! exports provided: Animal */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Animal", function() { return Animal; });
/* harmony import */ var _food__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./food */ "./src/js/food.js");



/**
 * On créer une classe Animal qui aura une propriété hunger pour
 * indiquer la faim de l'animal.
 */
class Animal {
    constructor(hunger = 0) {
        this.hunger = hunger;

    }
    /**
     * Cette méthode permet de faire baisser la faim de l'animal
     * selon ce qu'il mange.
     * @param {Food} food la nourriture que l'animal va manger
     */
    eat(food) {
        //Ici, on dit à notre méthode que si l'argument food fourni
        //n'est pas une instance de la classe Food, alors on fait 
        //planter le programme avec un throw new Error
        if( !(food instanceof _food__WEBPACK_IMPORTED_MODULE_0__["Food"]) ) {
            throw new Error('Invalid argument, Food instance exepected');
        }
        this.hunger -= food.calories;
        // this.hunger = this.hunger - food.calories;
        if(this.hunger < 0) {
            this.hunger = 0;
        }
    }
}

/***/ }),

/***/ "./src/js/cow.js":
/*!***********************!*\
  !*** ./src/js/cow.js ***!
  \***********************/
/*! exports provided: Cow */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Cow", function() { return Cow; });
/* harmony import */ var _herbivor__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./herbivor */ "./src/js/herbivor.js");
/* harmony import */ var _food__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./food */ "./src/js/food.js");



class Cow extends _herbivor__WEBPACK_IMPORTED_MODULE_0__["Herbivor"] {

    constructor(paramCalf = false) {
        super();
        this.hasCalf = paramCalf;
    }
    /**
     * Cette méthode permet à la vache de produire du lait si elle
     * a un veau.
     * @returns {Food} Si la vache a un veau, la méthode return une
     * instance de nourriture (du lait), sinon elle return null
     */
    milk() {
        let milk = null;
        if(this.hasCalf) {
           milk = new _food__WEBPACK_IMPORTED_MODULE_1__["Food"](140, false);
            this.hunger += 140;
        }
        
        return milk;

    }


}


/***/ }),

/***/ "./src/js/food.js":
/*!************************!*\
  !*** ./src/js/food.js ***!
  \************************/
/*! exports provided: Food */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Food", function() { return Food; });

class Food {
    constructor(calories, vegetal, cooked = false) {
        this.calories = calories;
        this.vegetal = vegetal;
        this.cooked = cooked;
    }
    /**
     * Méthode qui fait cuire la nourriture si elle n'est pas déjà
     * cuite. La cuisson augmente légérement les calories.
     */
    cook() {
        if(!this.cooked) {
        // if(this.cooked === false) {
           this.calories += 15;
           this.cooked = true;
        } else {
            console.log("already cooked");
        }
    }
}

/***/ }),

/***/ "./src/js/herbivor.js":
/*!****************************!*\
  !*** ./src/js/herbivor.js ***!
  \****************************/
/*! exports provided: Herbivor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Herbivor", function() { return Herbivor; });
/* harmony import */ var _animal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./animal */ "./src/js/animal.js");


class Herbivor extends _animal__WEBPACK_IMPORTED_MODULE_0__["Animal"] {
    /**
     * On redéfinit ici la méthode Animal.eat pour faire que si 
     * l'Animal est une instance de Herbivor, alors il ne mangera
     * que de la nourriture végétale.
     * @param {Food} food la nourriture à manger, on part du principe
     * qu'il nous sera donnée ici une instance de la classe Food,
     * mais en soit rien n'y oblige dans notre code actuel.
     */
    eat(food) {
        if(food.vegetal) {
          super.eat(food);
        } else {
            console.log('I only eat vegetables');
        }
    }
}

/***/ }),

/***/ "./src/js/index.js":
/*!*************************!*\
  !*** ./src/js/index.js ***!
  \*************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _food__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./food */ "./src/js/food.js");
/* harmony import */ var _cow__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./cow */ "./src/js/cow.js");





// let animalInstance = new Animal();
let carrot = new _food__WEBPACK_IMPORTED_MODULE_0__["Food"](20, true);

let cow = new _cow__WEBPACK_IMPORTED_MODULE_1__["Cow"](true);

let milk = cow.milk();

console.log(milk);


// let instanceShark = new SharkTiger();
// instanceShark.swim();

// console.log(instanceShark.hunger);
// instanceShark.eat(carrot);
// console.log(instanceShark);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL2pzL2FuaW1hbC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvanMvY293LmpzIiwid2VicGFjazovLy8uL3NyYy9qcy9mb29kLmpzIiwid2VicGFjazovLy8uL3NyYy9qcy9oZXJiaXZvci5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvanMvaW5kZXguanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0Esa0RBQTBDLGdDQUFnQztBQUMxRTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLGdFQUF3RCxrQkFBa0I7QUFDMUU7QUFDQSx5REFBaUQsY0FBYztBQUMvRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaURBQXlDLGlDQUFpQztBQUMxRSx3SEFBZ0gsbUJBQW1CLEVBQUU7QUFDckk7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7O0FBR0E7QUFDQTs7Ozs7Ozs7Ozs7Ozs7OztBQ2xGZTs7O0FBR2Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLEtBQUs7QUFDcEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7Ozs7Ozs7Ozs7QUM5Qm1CO0FBQ0o7O0FBRWY7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsS0FBSztBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBOzs7QUFHQTs7Ozs7Ozs7Ozs7Ozs7OztBQzFCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7Ozs7Ozs7Ozs7O0FDcEJpQjs7QUFFakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsS0FBSztBQUNwQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsQzs7Ozs7Ozs7Ozs7Ozs7OztBQ2pCZTtBQUNEOzs7QUFHZDtBQUNBOztBQUVBOztBQUVBOztBQUVBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSIsImZpbGUiOiJtYWluLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9zcmMvanMvaW5kZXguanNcIik7XG4iLCJpbXBvcnQgeyBGb29kIH0gZnJvbSBcIi4vZm9vZFwiO1xuXG5cbi8qKlxuICogT24gY3LDqWVyIHVuZSBjbGFzc2UgQW5pbWFsIHF1aSBhdXJhIHVuZSBwcm9wcmnDqXTDqSBodW5nZXIgcG91clxuICogaW5kaXF1ZXIgbGEgZmFpbSBkZSBsJ2FuaW1hbC5cbiAqL1xuZXhwb3J0IGNsYXNzIEFuaW1hbCB7XG4gICAgY29uc3RydWN0b3IoaHVuZ2VyID0gMCkge1xuICAgICAgICB0aGlzLmh1bmdlciA9IGh1bmdlcjtcblxuICAgIH1cbiAgICAvKipcbiAgICAgKiBDZXR0ZSBtw6l0aG9kZSBwZXJtZXQgZGUgZmFpcmUgYmFpc3NlciBsYSBmYWltIGRlIGwnYW5pbWFsXG4gICAgICogc2Vsb24gY2UgcXUnaWwgbWFuZ2UuXG4gICAgICogQHBhcmFtIHtGb29kfSBmb29kIGxhIG5vdXJyaXR1cmUgcXVlIGwnYW5pbWFsIHZhIG1hbmdlclxuICAgICAqL1xuICAgIGVhdChmb29kKSB7XG4gICAgICAgIC8vSWNpLCBvbiBkaXQgw6Agbm90cmUgbcOpdGhvZGUgcXVlIHNpIGwnYXJndW1lbnQgZm9vZCBmb3VybmlcbiAgICAgICAgLy9uJ2VzdCBwYXMgdW5lIGluc3RhbmNlIGRlIGxhIGNsYXNzZSBGb29kLCBhbG9ycyBvbiBmYWl0IFxuICAgICAgICAvL3BsYW50ZXIgbGUgcHJvZ3JhbW1lIGF2ZWMgdW4gdGhyb3cgbmV3IEVycm9yXG4gICAgICAgIGlmKCAhKGZvb2QgaW5zdGFuY2VvZiBGb29kKSApIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcignSW52YWxpZCBhcmd1bWVudCwgRm9vZCBpbnN0YW5jZSBleGVwZWN0ZWQnKTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLmh1bmdlciAtPSBmb29kLmNhbG9yaWVzO1xuICAgICAgICAvLyB0aGlzLmh1bmdlciA9IHRoaXMuaHVuZ2VyIC0gZm9vZC5jYWxvcmllcztcbiAgICAgICAgaWYodGhpcy5odW5nZXIgPCAwKSB7XG4gICAgICAgICAgICB0aGlzLmh1bmdlciA9IDA7XG4gICAgICAgIH1cbiAgICB9XG59IiwiaW1wb3J0IHsgSGVyYml2b3IgfSBmcm9tIFwiLi9oZXJiaXZvclwiO1xuaW1wb3J0IHsgRm9vZCB9IGZyb20gXCIuL2Zvb2RcIjtcblxuZXhwb3J0IGNsYXNzIENvdyBleHRlbmRzIEhlcmJpdm9yIHtcblxuICAgIGNvbnN0cnVjdG9yKHBhcmFtQ2FsZiA9IGZhbHNlKSB7XG4gICAgICAgIHN1cGVyKCk7XG4gICAgICAgIHRoaXMuaGFzQ2FsZiA9IHBhcmFtQ2FsZjtcbiAgICB9XG4gICAgLyoqXG4gICAgICogQ2V0dGUgbcOpdGhvZGUgcGVybWV0IMOgIGxhIHZhY2hlIGRlIHByb2R1aXJlIGR1IGxhaXQgc2kgZWxsZVxuICAgICAqIGEgdW4gdmVhdS5cbiAgICAgKiBAcmV0dXJucyB7Rm9vZH0gU2kgbGEgdmFjaGUgYSB1biB2ZWF1LCBsYSBtw6l0aG9kZSByZXR1cm4gdW5lXG4gICAgICogaW5zdGFuY2UgZGUgbm91cnJpdHVyZSAoZHUgbGFpdCksIHNpbm9uIGVsbGUgcmV0dXJuIG51bGxcbiAgICAgKi9cbiAgICBtaWxrKCkge1xuICAgICAgICBsZXQgbWlsayA9IG51bGw7XG4gICAgICAgIGlmKHRoaXMuaGFzQ2FsZikge1xuICAgICAgICAgICBtaWxrID0gbmV3IEZvb2QoMTQwLCBmYWxzZSk7XG4gICAgICAgICAgICB0aGlzLmh1bmdlciArPSAxNDA7XG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgIHJldHVybiBtaWxrO1xuXG4gICAgfVxuXG5cbn1cbiIsIlxuZXhwb3J0IGNsYXNzIEZvb2Qge1xuICAgIGNvbnN0cnVjdG9yKGNhbG9yaWVzLCB2ZWdldGFsLCBjb29rZWQgPSBmYWxzZSkge1xuICAgICAgICB0aGlzLmNhbG9yaWVzID0gY2Fsb3JpZXM7XG4gICAgICAgIHRoaXMudmVnZXRhbCA9IHZlZ2V0YWw7XG4gICAgICAgIHRoaXMuY29va2VkID0gY29va2VkO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBNw6l0aG9kZSBxdWkgZmFpdCBjdWlyZSBsYSBub3Vycml0dXJlIHNpIGVsbGUgbidlc3QgcGFzIGTDqWrDoFxuICAgICAqIGN1aXRlLiBMYSBjdWlzc29uIGF1Z21lbnRlIGzDqWfDqXJlbWVudCBsZXMgY2Fsb3JpZXMuXG4gICAgICovXG4gICAgY29vaygpIHtcbiAgICAgICAgaWYoIXRoaXMuY29va2VkKSB7XG4gICAgICAgIC8vIGlmKHRoaXMuY29va2VkID09PSBmYWxzZSkge1xuICAgICAgICAgICB0aGlzLmNhbG9yaWVzICs9IDE1O1xuICAgICAgICAgICB0aGlzLmNvb2tlZCA9IHRydWU7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcImFscmVhZHkgY29va2VkXCIpO1xuICAgICAgICB9XG4gICAgfVxufSIsImltcG9ydCB7IEFuaW1hbCB9IGZyb20gXCIuL2FuaW1hbFwiO1xuXG5leHBvcnQgY2xhc3MgSGVyYml2b3IgZXh0ZW5kcyBBbmltYWwge1xuICAgIC8qKlxuICAgICAqIE9uIHJlZMOpZmluaXQgaWNpIGxhIG3DqXRob2RlIEFuaW1hbC5lYXQgcG91ciBmYWlyZSBxdWUgc2kgXG4gICAgICogbCdBbmltYWwgZXN0IHVuZSBpbnN0YW5jZSBkZSBIZXJiaXZvciwgYWxvcnMgaWwgbmUgbWFuZ2VyYVxuICAgICAqIHF1ZSBkZSBsYSBub3Vycml0dXJlIHbDqWfDqXRhbGUuXG4gICAgICogQHBhcmFtIHtGb29kfSBmb29kIGxhIG5vdXJyaXR1cmUgw6AgbWFuZ2VyLCBvbiBwYXJ0IGR1IHByaW5jaXBlXG4gICAgICogcXUnaWwgbm91cyBzZXJhIGRvbm7DqWUgaWNpIHVuZSBpbnN0YW5jZSBkZSBsYSBjbGFzc2UgRm9vZCxcbiAgICAgKiBtYWlzIGVuIHNvaXQgcmllbiBuJ3kgb2JsaWdlIGRhbnMgbm90cmUgY29kZSBhY3R1ZWwuXG4gICAgICovXG4gICAgZWF0KGZvb2QpIHtcbiAgICAgICAgaWYoZm9vZC52ZWdldGFsKSB7XG4gICAgICAgICAgc3VwZXIuZWF0KGZvb2QpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgY29uc29sZS5sb2coJ0kgb25seSBlYXQgdmVnZXRhYmxlcycpO1xuICAgICAgICB9XG4gICAgfVxufSIsIlxuaW1wb3J0IHsgRm9vZCB9IGZyb20gXCIuL2Zvb2RcIjtcbmltcG9ydCB7IENvdyB9IGZyb20gXCIuL2Nvd1wiO1xuXG5cbi8vIGxldCBhbmltYWxJbnN0YW5jZSA9IG5ldyBBbmltYWwoKTtcbmxldCBjYXJyb3QgPSBuZXcgRm9vZCgyMCwgdHJ1ZSk7XG5cbmxldCBjb3cgPSBuZXcgQ293KHRydWUpO1xuXG5sZXQgbWlsayA9IGNvdy5taWxrKCk7XG5cbmNvbnNvbGUubG9nKG1pbGspO1xuXG5cbi8vIGxldCBpbnN0YW5jZVNoYXJrID0gbmV3IFNoYXJrVGlnZXIoKTtcbi8vIGluc3RhbmNlU2hhcmsuc3dpbSgpO1xuXG4vLyBjb25zb2xlLmxvZyhpbnN0YW5jZVNoYXJrLmh1bmdlcik7XG4vLyBpbnN0YW5jZVNoYXJrLmVhdChjYXJyb3QpO1xuLy8gY29uc29sZS5sb2coaW5zdGFuY2VTaGFyayk7XG4iXSwic291cmNlUm9vdCI6IiJ9