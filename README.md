# Héritage
Projet Javascript pour expliquer l'héritage en POO pour la promo 6 de Simplon Lyon

## Utiliser le projet
1. Faire un git clone
2. Positionner le terminal dans le dossier du projet `cd heritage`
3. Installer les dépendances `npm install`
4. Lancer la compilation webpack `npm run dev`


## Exercices
### Mini TP Héritage :
1. Créer un fichier food.js avec une classe Food dedans
2. La classe Food va avoir comme propriétés : calories, vegetal, cooked(modifié)
3. Elle aura également une méthode cook() qui fera augmenter ses calories et passera sa valeur de cooked à true (faire qu'on ne puisse cuisiner la nourriture qu'une seule fois)
4. Faire un nouveau fichier herbivor.js qui contiendra une class Herbivor
5. Redéfinir la méthode eat de la classe herbivor pour faire que si la nourriture n'est pas végétale, il ne mange pas
6. Faire encore un autre fichier cow.js qui contiendra une class Cow qui hérite de Herbivor
7. Rajouter une propriété hasCalf à true ou false sur la vache
9. Rajouter une méthode milk() qui produira une instance de nourriture si la vache hasCalf à true

### TP Pseudo-Chess
#### I. Créer le pion et l'afficher
1. Créer un fichier pawn.js dans lequel on crée et on export une class Pawn
2. Ajouter à la classe pawn une propriété x et une propriété y qui représenteront la position du pion
3. Ajouter une méthode move() qui fera bouger le pion de 100 sur l'axe x
4. Ajouter une méthode draw() qui créera le HTML du pion, de la même manière qu'on l'avait fait pour le carré ici : https://gitlab.com/simplonlyon/P6/js-exercice-game/blob/master/src/js/square.js (remplacer l'id par une classe 'pawn')
5. Créer un fichier src/css/chess.css dans lequel vous aller faire une règle sur la classe .pawn qui représentera un pion par un carré noir (puis charger le css dans le html)
6.  Dans le fichier exercice-chess.html, ajouter une div avec un id "board"
7.  exercice-chess.js capturer la div #board avec un querySelector puis  faire une instance de pawn et la faire se déplacer puis l'appendChild sur la div board (comme ici ligne 8 https://gitlab.com/simplonlyon/P6/js-exercice-game/blob/master/src/js/v2.js )
8. Dans le exercice-chess.html, rajouter un button move
9. Dans le exercice-chess.js, rajouter un eventlistener sur ce bouton pour faire que quand on click dessus, l'instance de pion se déplace puis se draw

#### II. Plusieurs pions / selection
1. Dans exercice-chess.js créer une function generatePieces(count) qui va généré un tableau d'instance de Pawn et le return
* 1.1 Créer la fonction en lui mettant le paramètre count (qui représentera le nombre de pièce à faire)
* 1.2 Créer une variable avec un tableau vide dedans
* 1.3  Faire une boucle for qui fera autant de tour que la valeur de count
* 1.4 A chaque tour de boucle, faire une nouvelle instance de Pawn qui aura la valeur actuelle de la boucle comme y puis push cette instance dans le tableau
* 1.5 Faire un return du tableau en question
2. Créer une function drawBoard(pieces) qui mettra à zéro l'élément html #board puis fera une boucle for..of sur les pieces qu'on lui donne en argument et pour chacune des pieces, fera un appendChild du draw de la piece actuelle (en gros une fonction display dont le but sera de générer le html du plateau de jeu à partir d'un tableau de piece)
3. Commenter tout ce qui est hors de ces deux fonctions (à part les imports), puis, tout en haut, faire une variable myPieces et lui assigner comme valeur de résultat de la fonction generatePieces(5), et en dessous, faire un appel à drawBoard() en lui donnant myPieces comme argument
4. Faire qu'on puisse sélectionner une pièce
* 4.1 Créer une variable selectedPiece et lui assigner null comme valeur
* 4.2 Dans la fonction drawBoard, à l'intérieur de la boucle, modifier le code pour mettre le résultat du draw() de la piece à l'intérieur d'une variable, mettre cette variable dans le appendChild puis ajouter un event listener sur cette variable(modifié)
* 4.3 Dans cet event listener, faire que l'on assigne la pièce actuelle de la boucle à la variable selectedPiece

#### III. Plusieurs types de pions
1. Créer un nouveau fichier bishop.js qui va contenir une classe Bishop qui héritera de Pawn
2. Surcharger/redéfinir la méthode move() du Bishop pour faire qu'il se déplace en diagonale (donc X+1 et y+1)
3. Surcharger la méthode draw() du Bishop pour modifier l'affichage de celui ci
* 3.1 Utiliser la méthode du parent (super.draw()) pour récupérer le HTML du Pawn
* 3.2 Ajouter une classe css bishop à cet élément
* 3.3 Ajouter un "B" en textContent de l'élément
* 3.4 return l'élément
4. Dans le fichier chess.css, rajouter un style pour la classe .bishop qui fera qu'elle est ronde et qu'elle a son texte en blanc et en gras
5. Créer un nouveau fichier knight.js avec une classe Knight qui héritera de Pawn. Faire à peu près les mêmes étapes qu'au dessus, sauf que le knight bouge de x+3 et y+1. Et modifier son draw pour qu'il ait une classe css knight et un K en textContent (rajouter la classe dans le css etc.)
6. Dans le exercice-chess.js, modifier la fonction generatePieces() pour faire qu'elle crée un Knight et un Bishop qu'elle mettra dans le tableau avec les autres
7. Relancer l'appli pour voir si ça marche
8. Bonus : essayer de faire que la fonction generatePiece() crée des types de pion de manière random
9. Bonus 2 : Faire que quand on sélectionne une pièce, ça lui rajoute une classe css pour la différencier des autres
